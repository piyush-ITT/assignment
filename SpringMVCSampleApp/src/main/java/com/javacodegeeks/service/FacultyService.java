/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javacodegeeks.service;

import com.javacodegeeks.model.Faculty;
import com.javacodegeeks.model.Login;
import java.util.List;

/**
 *
 * @author piyush.tiwari
 */
public interface FacultyService 
{
    void register(Faculty faculty);
    public List < Faculty > findAll();
    public Faculty getFaculty(Login login);
}
