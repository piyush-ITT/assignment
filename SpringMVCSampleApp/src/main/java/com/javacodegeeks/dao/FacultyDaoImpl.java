/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javacodegeeks.dao;

import com.javacodegeeks.model.Faculty;
import com.javacodegeeks.model.Login;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author piyush.tiwari
 */
@Repository
@Transactional
//@Qualifier("facultyDao")
public class FacultyDaoImpl implements FacultyDao 
{
    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private HibernateTemplate  hibernateTemplate;
        
    // Inserting faculty information in database
    @Override
    public void register(Faculty faculty) 
    {
        sessionFactory.getCurrentSession().saveOrUpdate(faculty);
         //hibernateTemplate.save(faculty);
    }

    // Method selecting all the information and showing to the user after log in
    @Override
    public List<Faculty> findAll() 
    {
        return (List<Faculty>) sessionFactory.getCurrentSession().createCriteria(Faculty.class).list();
        //String hql = "FROM facultyInformation";
	//	return (List<Faculty>) hibernateTemplate.find(hql);
    }

    // Method checking authentication at login time
    @Override
    public Faculty getFaculty(Login login) 
    {
       return (Faculty) sessionFactory.getCurrentSession().get(Faculty.class, login.getUsername());
    }
}
