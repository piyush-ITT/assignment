/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javacodegeeks.service;

import com.javacodegeeks.controller.HomeController;
import com.javacodegeeks.dao.FacultyDao;
import com.javacodegeeks.model.Faculty;
import com.javacodegeeks.model.Login;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author piyush.tiwari
 */
@Service("facultyService")
@Transactional
public class FacultyServiceImpl implements FacultyService 
{
private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
    @Autowired
    FacultyDao facultyDao;


    @Override
    public void register(Faculty faculty) 
    {
        logger.info("inside the register process");
        facultyDao.register(faculty);
    }

    @Override
    public List<Faculty> findAll() 
    {
        return facultyDao.findAll();
    }	

    @Override
    public Faculty getFaculty(Login login) 
    {
        return facultyDao.getFaculty(login);
    }

    @Override
    public Faculty getFacultyData(String email) 
    {
        return facultyDao.getFacultyData(email);
    }

    @Override
    public void deleteFaculty(String email) 
    {
        facultyDao.deleteFaculty(email);
    }

    @Override
    public void updateFacultyData(Faculty faculty) 
    {
        facultyDao.updateFacultyData(faculty);
    }

}
