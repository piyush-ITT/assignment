package com.javacodegeeks.configuration;

import com.javacodegeeks.dao.FacultyDao;
import com.javacodegeeks.dao.FacultyDaoImpl;
import com.javacodegeeks.service.FacultyService;
import com.javacodegeeks.service.FacultyServiceImpl;
import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;



@Configuration
@EnableTransactionManagement
@EnableWebMvc
@ComponentScan({ "com.javacodegeeks.configuration" })           // Creating beans
@PropertySource(value = {"classpath:application.properties"})   // Integrating application.properties
public class HibernateConfiguration {
    
    @Autowired
    private Environment environment;
 
    /*
    Crating Persistent object 
    */
    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(new String[] { "com.javacodegeeks.model" });
        return sessionFactory;
     }
    /*
    Providing database connectivity
    */
    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
        dataSource.setUrl(environment.getRequiredProperty("jdbc.url"));
        dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
        dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
        return dataSource;
    }

    /*
    Setting Session factory
    */ 
    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory s) {
       HibernateTransactionManager txManager = new HibernateTransactionManager();
       txManager.setSessionFactory(s);
       return txManager;
    }
/*
    @Bean
    public HibernateTemplate getHibernateTemplate(SessionFactory sessionFactory)
      {
        HibernateTemplate hibernateTemplate = new HibernateTemplate();
        hibernateTemplate.setSessionFactory(sessionFactory);
        return hibernateTemplate;
      }
    */
    
    // Creating bean of faculty service
    @Bean(name = "facultyService")
    public FacultyService facultyService() 
    {
            return new FacultyServiceImpl();
    }
    // Creating bean of facultyDao
    @Bean(name = "facultyDao")
    public FacultyDao facultyDao()
    {
        return new FacultyDaoImpl();
    }
}

