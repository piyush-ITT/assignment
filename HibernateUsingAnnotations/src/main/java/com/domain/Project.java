package com.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/*
 * Project persistence class
 */
@Entity
@Table(name="project")
public class Project 
{	
	@Id @Column(name = "projectId")
	private String projectId;
	
	@Column(name = "projectName")
	private String projectName;
	
	@Column(name = "projectStatus")
	private String projectStatus;
	
	// Many to one association mapping; email as foreign key
	@ManyToOne
	@JoinColumn(name="email")
	private Faculty faculty;

	
	public Faculty getFaculty() 
	{
		return faculty;
	}
	public void setFaculty(Faculty faculty) 
	{
		this.faculty = faculty;
	}
	public String getProjectId() 
	{
		return projectId;
	}
	public void setProjectId(String projectId) 
	{
		this.projectId = projectId;
	}
	public String getProjectName() 
	{
		return projectName;
	}
	public void setProjectName(String projectName) 
	{
		this.projectName = projectName;
	}
	public String getProjectStatus() 
	{
		return projectStatus;
	}
	public void setProjectStatus(String projectStatus) 
	{
		this.projectStatus = projectStatus;
	}
}
