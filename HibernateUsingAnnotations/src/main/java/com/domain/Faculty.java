package com.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/*
 * Persistence class
 */
@Entity
@Table(name="facultyInformation")
@NamedNativeQueries({
    @NamedNativeQuery(
            name    =   "genderInformation",
            query   =   "SELECT first_name, last_name, email, password, phone, gender " +
                        "FROM facultyInformation " + "WHERE gender = :gender", 
                        resultClass=Faculty.class
    )})
public class Faculty 
{
	@Column(name = "first_name")
	private String first_name;
	
	@Column(name = "last_name")
	private String last_name;
	
	@Id @Column(name="email")
	private String email;
	
	@Column(name="password")
	private String password;
	
	@Column(name="phone")
	private String phone;
	
	@Column(name="gender")
	private String gender;
	
	//One to many relationship with Project
	@OneToMany(mappedBy="faculty", cascade = CascadeType.ALL)
	private Set<Project> project;

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public Set<Project> getProject() 
	{
		return project;
	}

	public void setProject(Set<Project> project) 
	{
		this.project = project;
	}
}
