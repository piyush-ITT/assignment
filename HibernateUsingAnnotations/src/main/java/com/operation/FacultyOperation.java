package com.operation;
import java.util.List;
import java.util.Scanner;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.query.Query;

import com.domain.Faculty;
import com.domain.Project;
import com.utility.HibernateUtil;

public class FacultyOperation 
{
	private static Scanner sc;
	private static SessionFactory factory;
	private static Session session;
	
	public static void main(String[] args) 
	 {
	 //creating configuration object  
     //Configuration cfg=new Configuration();  
     //cfg.configure("hibernate.cfg.xml"); //populates the data of the configuration file   
	  
      int i;
	  sc = new Scanner(System.in);
      do 
      {
    	  //Creating session factory object
    	  factory = HibernateUtil.getSessionFactory(); 	  
    	  
    	  //getting session object from session factory	  
    	  session = factory.openSession();
    	  
    	  // Creating criteria object for query 
    	  Criteria criteria=session.createCriteria(Faculty.class);    	  
    	  
    	  //getting transaction object from session object
    	  session.beginTransaction();
    	  
      		//Menu which will show to user initially
	        System.out.println("Welcome enter the following operation you want to perform on faculty database:-");
	        System.out.println("Enter 0 for Exit");
	        System.out.println("1. Insert");
	        System.out.println("2. SelectAll");
	        System.out.println("3. Update");
	        System.out.println("4. Delete");
	        System.out.println("5. SortedResult");
	        
	        Faculty faculty;
	        i = sc.nextInt();
	            switch(i)
	            {
		             case 1:		            	 		            	 		            	 
		            	 faculty = createFaculty();
		            	 Project project = projectInfo();
		            	 project.setFaculty(faculty);
		            	 session.save(project);    // Save project information into database
		            	 session.save(faculty);		// Save Faculty information into database            			            	 
		            	 session.getTransaction().commit();
		           	  	 System.out.println("Inserted Successfully");
		                 break;
		             case 2:
		            	 selectAll(session);
		            	 session.getTransaction().commit();
		            	 break;
		             case 3:
		            	 updateInformation(session);
		            	 session.getTransaction().commit();
		           	  	 System.out.println("Updated Successfully");
		            	 break;
		             case 4:
		            	 deleteFaculty(session);
		            	 session.getTransaction().commit();
		           	  	 System.out.println("Deleted Successfully");
		            	 break;
		             case 5:
		            	 sortFaculty(criteria);
		            	 session.getTransaction().commit();
		            	 break;
		             default:
		                 System.out.println("-----------invalid input-----------\n");
	
	            }
	    }while (i != 0);	
	  
	  session.close();
	  factory.close();
	 }

	
	// Method for taking input from user
	private static Project projectInfo()
	{
		//Create project entity object
		Project project = new Project();
		System.out.println("Enter project id:");
		project.setProjectId(sc.next());
		System.out.println("Enter project name:");
		project.setProjectName(sc.next());
		System.out.println("Enter project status:");
		project.setProjectStatus(sc.next());
		return project;
	}
	
	// Method for taking input from user
	private static Faculty createFaculty() 
	{
		//Create faculty entity object
		Faculty faculty = new Faculty();
		System.out.println("Enter your First name:");
		faculty.setFirst_name(sc.next());
		System.out.println("Enter your Last name:");
		faculty.setLast_name(sc.next());
		System.out.println("Enter your email id:");
		faculty.setEmail(sc.next());
		System.out.println("Enter password:");
		faculty.setPassword(sc.next());
		System.out.println("Enter your phone number:");
		faculty.setPhone(sc.next());
		System.out.println("Enter your Gender:");
		faculty.setGender(sc.next());
		return faculty;
	}

	private static void selectAll(Session session) 
	{
		 System.out.println("choose the table from which you want to see the information:-");
         System.out.println("1. facultyInformation");
         System.out.println("2. project");
         System.out.println("3. Information of male and female faculties separately");
         int i = sc.nextInt();
         if(i == 1)
         {
        	 // Making object of query which hold all the information stored in facultyInformation
        	 Query<Faculty> query = session.createQuery("from Faculty");
    		 List <Faculty> faculties = query.list();
    		 System.out.println("--------------------------------------------------------------------------------------------------------------");
    		 System.out.println("First name  |   Last name   |      Email        |     Password         |        Phone        |  Gender      ");
    		 System.out.println("--------------------------------------------------------------------------------------------------------------");
    		 for(Faculty faculty  : faculties )
    		 {
    			 System.out.println(faculty.getFirst_name() + "\t\t" + faculty.getLast_name() + "\t\t"
    					 + faculty.getEmail() + "\t\t" + faculty.getPassword() + "\t\t" 
    					 + faculty.getPhone() + "\t" + faculty.getGender());
    		 }		
         }
         if(i == 2)
         {
        	 // Printing the information using pagination
        	 Query<Project> query = session.createQuery("from Project");
        	 query.setFirstResult(0);
             query.setMaxResults(2);
    		 List <Project> projects = query.list();
    		 System.out.println("--------------------------------------------------------------------------------");
    		 System.out.println("Project Id |   Project Name   |      Project Status        |     Email         |");
    		 System.out.println("--------------------------------------------------------------------------------");
    		 for(Project project  : projects )
    		 {
    			 System.out.println(project.getProjectId() + "\t\t" + project.getProjectName() 
    			 + "\t\t    " + project.getProjectStatus() + "\t\t\t" + project.getFaculty().getEmail());
    		 }
    		 System.out.println("Remaining result on the next page:-");
    		 query.setFirstResult(2);
    		 List <Project> pro = query.list();
    		 System.out.println("--------------------------------------------------------------------------------");
    		 System.out.println("Project Id |   Project Name   |      Project Status        |     Email         |");
    		 System.out.println("--------------------------------------------------------------------------------");
    		 for(Project project  : pro )
    		 {
    			 System.out.println(project.getProjectId() + "\t\t" + project.getProjectName() 
    			 + "\t\t    " + project.getProjectStatus() + "\t\t\t" + project.getFaculty().getEmail());
    		 }
         }
         if(i == 3)
         {
        	 // Printing information using named query
        	 System.out.println("Enter Gender to check how many male or female faculties exists in Faculty Database:");
        	 String str = sc.next();
        	 List<Faculty> faculties = session.createNamedQuery("genderInformation", Faculty.class)
                    				    .setParameter("gender", str)
        			 					.getResultList();
        	 /*Query query = session.createSQLQuery(
        			 "select * from facultyInformation where gender = :gender")
        			 .addEntity(Faculty.class)
        			 .setParameter("gender", sc.next());
        			 List<Faculty> result = query.list();*/
        			 System.out.println("--------------------------------------------------------------------------------------------------------------");
            		 System.out.println("First name  |   Last name   |      Email        |     Password         |        Phone        |    Gender      ");
            		 System.out.println("--------------------------------------------------------------------------------------------------------------");
            		 for(Faculty faculty  : faculties )
            		 {
            			 System.out.println(faculty.getFirst_name() + "\t\t" + faculty.getLast_name() + "\t\t"
            					 + faculty.getEmail() + "\t\t" + faculty.getPassword() + "\t\t" 
            					 + faculty.getPhone() + "\t" + faculty.getGender());
            		 }		
         }
		
	}
	/*
	 * Method for updating information in database
	 */
	private static void updateInformation(Session session) 
	{
		System.out.println("choose among the following which you want to update:-");
        System.out.println("1. First Name");
        System.out.println("2. Last Name");
        System.out.println("3. Password");
        System.out.println("4. Phone Number");
        int i = sc.nextInt();
        System.out.println("Enter your email id:");
        String str = sc.next();
        Faculty faculty = (Faculty)session.get(Faculty.class, str);
        // menu for user that what he/she wants to update
        switch(i)
        {
        case 1:
        	System.out.println("please enter your new First Name:");
        	faculty.setFirst_name(sc.next());
            break;
        case 2:
        	System.out.println("please enter your new Last Name:");
        	faculty.setLast_name(sc.next());
       	 	break;
        case 3:
        	System.out.println("please enter your new Password:");
        	faculty.setPassword(sc.next());
       	 	break;
        case 4:
        	System.out.println("please enter your new Phone Number:");
        	faculty.setPhone(sc.next());
       	 	break;
        default:
            System.out.println("-----------invalid input-----------\n");
        }
	}
	/*
	 * Method to delete information from database
	 */
	private static void deleteFaculty(Session session) 
	{
		System.out.println("enter the email id of faculty whose information you want to delete:");
		Faculty faculty1 = new Faculty();
		faculty1.setEmail(sc.next());
		session.delete(faculty1);		
	}
	
	private static void sortFaculty(Criteria criteria) 
	{
		//criteria.addOrder(Order.asc("first_name")); 
		/*CriteriaQuery<Student3> cq = session.getCriteriaBuilder().createQuery(Student3.class);
		cq.from(Student3.class);
		List<Student3> studentsNameList = session.createQuery(cq).getResultList();

		for (Student3 studentName : studentsNameList) {
		    System.out.println("Student details - "+studentName.getName()+" -- "+studentName.getEmail());   
		}*/
		
		// Storing the faculty information in descending order
   	 	List<Faculty> list=criteria.addOrder(Order.desc("first_name")).list();
	   	 System.out.println("--------------------------------------------------------------------------------------------------------------");
		 System.out.println("First name  |   Last name   |      Email        |     Password         |        Phone        |  Gender      ");
		 System.out.println("--------------------------------------------------------------------------------------------------------------");
		 for(Faculty faculty  : list )
		 {
			 System.out.println(faculty.getFirst_name() + "\t\t" + faculty.getLast_name() 
			 + "\t\t" + faculty.getEmail() + "\t\t" + faculty.getPassword() + "\t\t" 
			 + faculty.getPhone() + "\t" + faculty.getGender());
		 }		
			
	}
	
}
