
package com.facultyDao;

import com.restapiassignment.Faculty;
import com.utility.HibernateUtil;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author piyush.tiwari
 */
public class FacultyDao 
{
    SessionFactory sessionFactory;
    /*
    Registering the faculty in database
    */
    public void register(Faculty faculty) 
    {
        Session session = HibernateUtil.getSession();        
        Transaction tx = session.beginTransaction();
        session.save(faculty);        
        tx.commit();
        session.close();
    }
   
    /*
    Getting all the information of faculty from database
    */
    public List<Faculty> findAll() 
    {
        Session session = HibernateUtil.getSession();   
        Query query = session.createQuery("from Faculty");
        List<Faculty> faculties =  query.list();
        session.close();
        return faculties;
    }

    /*
    Selecting specific faculty information from database
    */
    public Faculty getFacultyData(String email)
    {
       Session session = HibernateUtil.getSession();
       Transaction tx = session.beginTransaction();
       Faculty faculty =(Faculty)session.get(Faculty.class, email);
       tx.commit();
       session.close();
       return faculty; 
    }
    
    /*
    Deleting faculty information from database
    */
     public void deleteFaculty(String email) 
    {
        Session session = HibernateUtil.getSession();
        Transaction tx = session.beginTransaction();
        Faculty faculty=new Faculty();
        faculty.setEmail(email);
        session.delete(faculty);
        tx.commit();
        session.close();
    }

     /*
     Updating faculty information in database
     */
    public void updateFacultyData(Faculty faculty) 
    {
        Session session = HibernateUtil.getSession();
        Transaction tx = session.beginTransaction();
        session.update(faculty);
        tx.commit();
        session.close();
    }
    
}

