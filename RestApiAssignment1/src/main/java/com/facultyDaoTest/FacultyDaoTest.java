package com.facultyDaoTest;

import static org.junit.Assert.*;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import com.facultyDao.FacultyDao;
import com.restapiassignment.Faculty;
import com.utility.HibernateUtil;

public class FacultyDaoTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
		System.out.println("-----> SETUP <-----");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception 
	{
		System.out.println("-----> DESTROY <-----");
	}

	@Test
	public void testRegisterPass() 
	{
		Faculty faculty = new Faculty();
        faculty.setFirst_name("piysh");
        faculty.setLast_name("tiwari");
        faculty.setEmail("p@gmail.com");
        faculty.setPassword("1234567890");
        faculty.setPhone("1234567890");
        faculty.setGender("male");
		if(faculty != null) 
		{
			assertNotNull("faculty first name can't be null", faculty.getFirst_name());
            assertNotNull("faculty Last name can't be null", faculty.getLast_name());
			assertNotNull("Email must not be empty", faculty.getEmail());
            assertNotNull("faculty Password can't be null", faculty.getPassword());
            assertNotNull("faculty phone number can't be null", faculty.getPhone());
            assertNotNull("faculty gender can't be null", faculty.getGender());
		}
		
		assertNotNull("New Faculty Information is null", faculty);
	}

	@Test
	public void testRegisterFail() 
	{
		Faculty faculty = new Faculty();
        faculty.setLast_name("tiwari");
        faculty.setEmail("p@gmail.com");
        faculty.setPassword("1234567890");
        faculty.setPhone("1234567890");
        faculty.setGender("male");
		if(faculty != null) 
		{
			assertNotNull("faculty first name can't be null", faculty.getFirst_name());
            assertNotNull("faculty Last name can't be null", faculty.getLast_name());
			assertNotNull("Email must not be empty", faculty.getEmail());
            assertNotNull("faculty Password can't be null", faculty.getPassword());
            assertNotNull("faculty phone number can't be null", faculty.getPhone());
            assertNotNull("faculty gender can't be null", faculty.getGender());
		}
		
		assertNotNull("New Faculty Information is not null", faculty);
	}
	
	@Test(expected=org.hibernate.id.IdentifierGenerationException.class)
	public void IdentifierGenerationExceptionTest()
	{
		Faculty faculty = new Faculty();
		FacultyDao facultyDao = new FacultyDao();
		facultyDao.register(faculty);
	}
	
	@Test
	public void testFindAll() 
	{
		Session session = HibernateUtil.getSession();   
        Query query = session.createQuery("from Faculty");
        List<Faculty> faculties =  query.list();
        assertNotNull("faculty object is null", faculties);
        session.close();
	}
	
	@Test
	public void testGetFacultyDataPass() 
	{
	   String email = "pappu@gmail.com";
	   Session session = HibernateUtil.getSession();
       Transaction tx = session.beginTransaction();
       Faculty faculty =(Faculty)session.get(Faculty.class, email);
       if(faculty != null) 
		{
    	   assertNotNull("faculty first name can't be null", faculty.getFirst_name());
           assertNotNull("faculty Last name can't be null", faculty.getLast_name());
           assertNotNull("Email must not be empty", faculty.getEmail());
           assertNotNull("faculty Password can't be null", faculty.getPassword());
           assertNotNull("faculty phone number can't be null", faculty.getPhone());
           assertNotNull("faculty gender can't be null", faculty.getGender());
		}
		
       assertNotNull("faculty object is null", faculty);
       tx.commit();
       session.close();
	}
	
	@Test
	public void testGetFacultyDataFail() 
	{
	   String email = "p@gmail.com";
	   Session session = HibernateUtil.getSession();
       Transaction tx = session.beginTransaction();
       Faculty faculty =(Faculty)session.get(Faculty.class, email);
       if(faculty != null) 
		{
    	   assertNotNull("faculty first name can't be null", faculty.getFirst_name());
           assertNotNull("faculty Last name can't be null", faculty.getLast_name());
           assertNotNull("Email must not be empty", faculty.getEmail());
           assertNotNull("faculty Password can't be null", faculty.getPassword());
           assertNotNull("faculty phone number can't be null", faculty.getPhone());
           assertNotNull("faculty gender can't be null", faculty.getGender());
		}
		
       assertNotNull("faculty object is null", faculty);
       tx.commit();
       session.close();
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testGetFacultyData() 
	{
	   Faculty faculty = new Faculty();
	   Session session = HibernateUtil.getSession();
       Transaction tx = session.beginTransaction();
       faculty =(Faculty)session.get(Faculty.class, faculty.getEmail());
       tx.commit();
       session.close();
	}

	@Test
	public void testDeleteFacultyPass() 
	{
	   String email = "sur@gmail.com";
	   Session session = HibernateUtil.getSession();
       Transaction tx = session.beginTransaction();
       Faculty faculty=new Faculty();
       faculty.setEmail(email);
       //session.delete(faculty);
       faculty =(Faculty)session.get(Faculty.class, email);                 	      
       assertNotNull("faculty object is null", faculty);
       tx.commit();
       session.close();
	}

	@Test
	public void testUpdateFacultyData() 
	{
		
	}

}
