package com.JdbcDatabase;

public class EmployeeDepartment 
{
	private int employeeDapartmentId;
	private int departmentNumber;
	private String fromTheDate;
	private String toTheDate;
	
	public int getEmployeeDapartmentId() 
	{
		return employeeDapartmentId;
	}
	public void setEmployeeDapartmentId(int employeeDapartmentId) 
	{
		this.employeeDapartmentId = employeeDapartmentId;
	}
	public int getDepartmentNumber() 
	{
		return departmentNumber;
	}
	public void setDepartmentNumber(int departmentNumber) 
	{
		this.departmentNumber = departmentNumber;
	}
	public String getFromTheDate() 
	{
		return fromTheDate;
	}
	public void setFromTheDate(String fromTheDate) 
	{
		this.fromTheDate = fromTheDate;
	}
	public String getToTheDate() 
	{
		return toTheDate;
	}
	public void setToTheDate(String toTheDate) 
	{
		this.toTheDate = toTheDate;
	}
}
