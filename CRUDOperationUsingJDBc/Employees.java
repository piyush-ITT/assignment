package com.JdbcDatabase;

public class Employees 
{
	private int employeeId;
	private String firstName;
	private String lastName;
	private String birthDate;
	private String gender;
	private String hireDate;
	
	public int getEmployeeId() 
	{
		return employeeId;
	}
	public void setEmployeeId(int employeeId) 
	{
		this.employeeId = employeeId;
	}
	public String getFirstName() 
	{
		return firstName;
	}
	public void setFirstName(String firstName) 
	{
		this.firstName = firstName;
	}
	public String getLastName() 
	{
		return lastName;
	}
	public void setLastName(String lastName) 
	{
		this.lastName = lastName;
	}
	public String getBirthDate() 
	{
		return birthDate;
	}
	public void setBirthDate(String birthDate) 
	{
		this.birthDate = birthDate;
	}
	public String getGender() 
	{
		return gender;
	}
	public void setGender(String gender) 
	{
		this.gender = gender;
	}
	public String getHireDate() 
	{
		return hireDate;
	}
	public void setHireDate(String hireDate) 
	{
		this.hireDate = hireDate;
	}	

}
