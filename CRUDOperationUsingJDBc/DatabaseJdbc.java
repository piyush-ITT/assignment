package com.JdbcDatabase;

import java.util.InvalidPropertiesFormatException;
import java.util.Properties;
import java.util.Scanner;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;

public class DatabaseJdbc 
{
	private static Scanner sc;
	static Properties properties;
	static Connection connection;
	
	public static void main(String[] args) throws SQLException, InvalidPropertiesFormatException, IOException 
	{
		try 
		{
		 // Drivers 
		  Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) 
		{
			System.out.println("Where is your MySQL JDBC Driver?");
			e.printStackTrace();
			return;
		}
		System.out.println("MySQL JDBC Driver Registered!");
		
		File configurationFile = new File("C:\\Users\\piyush.tiwari\\workspace\\JdbcDatabase\\src\\main\\java\\com\\JdbcDatabase\\config.properties");
	    InputStream configFileReader = new FileInputStream(configurationFile);
	    properties = new Properties();
	    properties.loadFromXML(configFileReader);
	    try
	    {
	    	connection = DriverManager.getConnection(
					properties.getProperty("databaseString")
					+ properties.getProperty("host") + ":"
					+ properties.getProperty("port") + "/"
					+ properties.getProperty("database"),
					properties.getProperty("user"),
					properties.getProperty("password"));    	
	    }catch (SQLException e) 
		{
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;
		}
	    
		int i;
		
		new CRUDOperation(connection);
        sc = new Scanner(System.in);
        do 
        {
        	//Menu which will show to user initially
	        System.out.println("Welcome enter the following operation you want to perform on database:-");
	        System.out.println("Enter 6 for Exit");
	        System.out.println("1. Create");
	        System.out.println("2. Insert");
	        System.out.println("3. Select");
	        System.out.println("4. Update");
	        System.out.println("5. Delete");
	        i = sc.nextInt();
	            switch(i)
	            {
		             case 1:
		              	 CRUDOperation.createUserDbTable(); // Method for Create table
		                 break;
		             case 2:
		            	 CRUDOperation.insertRow(); // Method for Insert row
		                 break;
		             case 3:
		            	 CRUDOperation.selectData();// Method for Select data
		            	 break;
		             case 4:
		            	 CRUDOperation.updateData();// Method for Update
		            	 break;
		             case 5:
		            	 CRUDOperation.deleteUserDbTable();// Method for deleting table
		            	 break;
		             case 6:
		            	 System.exit(1);
		             default:
		                 System.out.println("-----------invalid input-----------\n");
	
	            }
	    }while (i != 0);
    }
	
}
