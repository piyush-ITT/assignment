package com.JdbcDatabase;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class CRUDOperation
{
		static Scanner sc = new Scanner(System.in);
		static Connection connection;
	
			public CRUDOperation(Connection connection1) 
			{
			// TODO Auto-generated constructor stub
				connection = connection1;
		}

			// Definition for Method to create table
			public static void createUserDbTable() throws SQLException 
			{
				int flag=0,flag1=0;
				String column_name=null;
				String data_type=null;
				String all_column="";
				// TODO Auto-generated method stub
				System.out.println("Enter table name you want to create:");
				sc.nextLine();
				String table_name = sc.nextLine();
				System.out.println("Enter no. of column you want to create in table:");
				int no_of_column = sc.nextInt();
				sc.nextLine();
				for(int k=1;k<no_of_column;k++)
				{
					System.out.println("Enter the Column name:");			
		        	column_name = sc.nextLine();
		        	System.out.println("Enter column data type in given format: Data_type/Data_type(size)");
		        	data_type = sc.nextLine();
					all_column=all_column+" "+column_name+" "+data_type+",";
				}
				System.out.println("Enter the Column name:");			
		    	column_name = sc.nextLine();
		    	System.out.println("Enter column data type in given format: Data_type/Data_type(size)");
		    	data_type = sc.nextLine();
					
		    	 String str,primary_key = null;
		         System.out.println("Do you want to make primary key?(y/n)");
		         str = sc.next();
		         if(str.equalsIgnoreCase("y"))
		         {
		         	System.out.println("Enter the Column name which you want to make primary key:");
		         	sc.nextLine();
		         	primary_key = sc.nextLine();
		         	flag=1;
		         }
		         
		         String str1,foreign_key = null,foreign_table = null,foreign_column = null;
		         System.out.println("Do you want to add foreign key?(y/n)");
		         str1 = sc.next();
		         if(str1.equalsIgnoreCase("y"))
		         {
		         	System.out.println("Enter the Column name which you want to make foreign key:");
		         	sc.nextLine();
		         	foreign_key = sc.nextLine();
		         	System.out.println("Enter the table name which you want to connect:");
		         	foreign_table = sc.nextLine();
		         	System.out.println("Enter the Column name of foreign table:");
		         	foreign_column = sc.nextLine();
		         	flag1=1;
		         }
		         
		         Statement statement = null;
		         String createTableSQL=null;             
		         if(flag==1 && flag1==1)
		         {
		         createTableSQL = "CREATE TABLE " + table_name + "("
		 				        + all_column +column_name+" "+data_type+" ,PRIMARY KEY("+primary_key+"), "
		 				       	+ "FOREIGN KEY("+foreign_key+") "+ "REFERENCES " 
		 						+ foreign_table+"("+foreign_column+"));";
		         }
		         else if(flag==1)
		         {
		        	 createTableSQL = "CREATE TABLE " + table_name + "("
		      						+ all_column +column_name+" "+data_type+" ,PRIMARY KEY("+primary_key+"));";
		        	 
		         }
		         else if(flag1==1)
		         {
		        	 createTableSQL = "CREATE TABLE " + table_name + "("
		       						+ all_column + column_name+" "+data_type+" ,FOREIGN KEY("+foreign_key+") "
		       						+ "REFERENCES " 
		     						+ foreign_table+"("+foreign_column+"));"; 
		         }
		         
		         else if(flag==0)
		         {
		        	 createTableSQL = "CREATE TABLE " + table_name + "("
		      				+ all_column+column_name+" "+data_type+");";
		        	 
		         }
		         
		         statement = connection.createStatement();

				System.out.println(createTableSQL);
		                     // execute the SQL statement
				statement.execute(createTableSQL);

				System.out.println("-----Table is created!------\n");
		 

				if (statement != null)
				{
					statement.close();
				}
			}
			
			// Definition for Method to insert new row
			public static void insertRow() 
			{
				// TODO Auto-generated method stub
				 System.out.println("Enter name of Table");
			       // sc.nextLine();
			        String tableName = sc.nextLine();
			        if (tableName.equalsIgnoreCase("employees")) {
			            employees();
			        } else if (tableName.equalsIgnoreCase("emp_dept")) {
			            empDept();
			        } else if (tableName.equalsIgnoreCase("dept_manager")) {
			            deptManager();
			        } else if (tableName.equalsIgnoreCase("departments")) {
			            departments();
			        }
			}

			public static void departments() 
			{
				// TODO Auto-generated method stub
				Departments departments = new Departments();
		        String str = "Insert into departments (dept_no,dept_name) values (?,?)";
		        System.out.println("Enter Department number");
		        departments.setDepartmentNo(sc.nextInt());
		        System.out.println("Enter Department Name");
		        sc.nextLine();
		        departments.setNameOfDepartment(sc.nextLine());
		        try {
		            PreparedStatement ps = connection.prepareStatement(str);
		            ps.setInt(1, departments.getDepartmentNo());
		            ps.setString(2, departments.getNameOfDepartment());
		            ps.executeUpdate();
		            System.out.println("-----data inserted!-----\n");
		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		        }
			}

			public static void deptManager() 
			{
				// TODO Auto-generated method stub
				DepartmentManager departmentManager = new DepartmentManager();
		        String str = "Insert into dept_manager (manager_dept_no,manager_employee_id,from_date,to_date) values (?,?,?,?)";
		        System.out.println("Enter department number");
		        departmentManager.setManagerDeptNumber(sc.nextInt());
		        System.out.println("Enter manager_employee_id");
		        departmentManager.setEmployeeManagerId(sc.nextInt());
		        System.out.println("Enter the joining date");
		        sc.nextLine();
		        departmentManager.setFromDatee(sc.nextLine());
		        System.out.println("Enter the last working date");
		        departmentManager.setToDatee(sc.nextLine());
		        System.out.println(str);
		        try 
		        {      	
		            PreparedStatement ps = connection.prepareStatement(str);
		            ps.setInt(1, departmentManager.getManagerDeptNumber());
		            ps.setInt(2, departmentManager.getEmployeeManagerId());
		            ps.setDate(3, Date.valueOf(departmentManager.getFromDatee()));
		            ps.setDate(4, Date.valueOf(departmentManager.getToDatee()));
		            ps.executeUpdate();
		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		        }
			}

			public static void empDept() 
			{
				// TODO Auto-generated method stub
				EmployeeDepartment employeeDepartment = new EmployeeDepartment();
		        String str = "Insert into emp_dept (department_employee_id,dept_dept_no,from_date,to_date) values (?,?,?,?)";
		        System.out.println("Enter Employee Id");
		        employeeDepartment.setEmployeeDapartmentId(sc.nextInt());       
		        System.out.println("Enter Department Id");
		        employeeDepartment.setEmployeeDapartmentId(sc.nextInt());
		        System.out.println("Enter the joining date of Employee");
		        sc.nextLine();
		        employeeDepartment.setFromTheDate(sc.nextLine());
		        System.out.println("Enter the last workingdate");
		        employeeDepartment.setToTheDate(sc.nextLine());

		        try {
		            PreparedStatement ps = connection.prepareStatement(str);
		            ps.setInt(1, employeeDepartment.getEmployeeDapartmentId());
		            ps.setInt(2, employeeDepartment.getDepartmentNumber());
		            ps.setDate(3, Date.valueOf(employeeDepartment.getFromTheDate()));
		            ps.setDate(4, Date.valueOf(employeeDepartment.getToTheDate()));
		            ps.executeUpdate();
		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		        }
			}

			public static void employees() 
			{
				// TODO Auto-generated method stub
				Employees employees = new Employees();
		        String str = "Insert into employees (first_name,last_name,birth_date,gender,hire_date) values (?,?,?,?,?)";
		        System.out.println("Enter the first name of Employee");
		        sc.nextLine();
		        employees.setFirstName(sc.nextLine());
		        System.out.println("Enter the last name of Employee");
		        employees.setLastName(sc.nextLine());
		        System.out.println("Enter the birth date of Employee");
		        employees.setBirthDate(sc.nextLine());
		        System.out.println("Enter the gender of Employee");
		        employees.setGender(sc.nextLine());
		        System.out.println("Enter the hire date of Employee");
		        employees.setHireDate(sc.nextLine());    

		        try {
		            PreparedStatement ps = connection.prepareStatement(str);
		            ps.setDate(3, Date.valueOf(employees.getBirthDate()));
		            ps.setString(1, employees.getFirstName());
		            ps.setString(2, employees.getLastName());
		            ps.setString(4, employees.getGender());
		            ps.setDate(5, Date.valueOf(employees.getHireDate()));
		            ps.executeUpdate();
		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		        }
			}	
			// InsertRow all methods end here
			
			
			
			 // Definition for Method to select data
			public static void selectData() 
			{
				// TODO Auto-generated method stub
				String table_name, str;
		        System.out.println("Enter table Name");
		        table_name = sc.nextLine();
		        str = "select *from " + table_name + "";
		        try {
		            PreparedStatement ps = connection.prepareStatement(str);
		            ResultSet rs = ps.executeQuery();
		            if (table_name.equals("employees")) {
		                System.out.println("-------------+---------------+-----------+----------+--------+--------------+\n"
					                        + " employee_id | first_name | last_name | birth_date | gender | hire_date |\n"
					                        + "-------------+---------------+-----------+----------+--------+--------------+");
		                while (rs.next()) {
		                    System.out.println(rs.getInt("employee_id") + "\t" + "\t" + rs.getString("first_name") + "\t" + rs.getString("last_name") + rs.getDate("birth_date") + "\t" + rs.getString("gender") + "\t" + rs.getDate("hire_date"));
		                }
		            }  else if (table_name.equals("departments")) {
		                System.out.println("+---------------+----------+\n"
					                        + "| dept_no | dept_name   |\n"
					                        + "+---------------+-------+");
		                while (rs.next()) {
		                    System.out.println("\t" + rs.getInt("dept_no") + "\t" + rs.getString("dept_name"));
		                }
		            } else if (table_name.equals("dept_manager")) {
		                System.out.println("+----------------------+--------------------------+------------+------------+\n"
					                        + "| manager_dept_no | manager_employee_id | from_date  | to_date    |\n"
					                        + "+----------------------+--------------------------+------------+------------+");
		                while (rs.next()) {
		                    System.out.println(rs.getInt("manager_dept_no") + "\t" + rs.getInt("manager_employee_id") + "\t" + rs.getDate("from_date") + "\t" + rs.getDate("to_date"));
		                }
		            } else if (table_name.equals("emp_dept")) {

		                System.out.println("+---------------------------------+-----------------------------------+------------+------------+\n"
					                        + "| dept_employee_Id | dept_dept_no | from_date  | to_date    |\n"
					                        + "+---------------------------------+-----------------------------------+------------+------------+");
		                while (rs.next()) {
		                    System.out.println("\t\t" + rs.getInt("dept_employee_Id") + "\t\t\t" + rs.getInt("dept_dept_no") + "\t\t" + rs.getDate("from_date") + "\t" + rs.getDate("to_date"));
		                }
		            }
		            System.out.println(str);

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		        }

			}
	// Definition for Method to Update
		public static void updateData()
		{
			// TODO Auto-generated method stub
			String table_name, column_name, primary_key, data, primaryKeyValue;
	        System.out.println("Enter name of Table");
	        table_name = sc.nextLine();
	        System.out.println("Enter column  name");
	        column_name = sc.nextLine();
	        System.out.println("Enter data");
	        data = sc.nextLine();
	        System.out.println("Enter primary key column name");
	        primary_key = sc.nextLine();
	        System.out.println("Enter primary key column value");
	        primaryKeyValue = sc.nextLine();
	        String str = "update " + table_name + " set " + column_name + " ='" + data + "' where " + primary_key + " = " + primaryKeyValue;

	        System.out.println(str);
	        try 
	        {
	            PreparedStatement ps = connection.prepareStatement(str);
	            ps.executeUpdate();
	        } catch (SQLException ex) 
	        {
	            System.out.println(ex.getMessage());
	        }
	    }
		// Definition for Method to delete table
	    public static void deleteUserDbTable() 
	    {
	        String table_name;
	        System.out.println("Enter name of Table");
	        table_name = sc.next();
	        String str = "drop table " + table_name;
	        try
	        {
	            PreparedStatement ps = connection.prepareStatement(str);
	            ps.executeUpdate();
	        } catch (SQLException ex)
	        {
	            System.out.println(ex.getMessage());
	        }
		}		

}
