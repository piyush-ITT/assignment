package com.JdbcDatabase;

public class Departments 
{
	private int departmentNo;
	private String nameOfDepartment;
	
	public int getDepartmentNo() 
	{
		return departmentNo;
	}
	public void setDepartmentNo(int departmentNo) 
	{
		this.departmentNo = departmentNo;
	}
	public String getNameOfDepartment() 
	{
		return nameOfDepartment;
	}
	public void setNameOfDepartment(String nameOfDepartment) 
	{
		this.nameOfDepartment = nameOfDepartment;
	}

}
