package com.JdbcDatabase;

public class DepartmentManager 
{
	private int employeeManagerId;
	private int managerDeptNumber;
	private String fromDatee;
	private String toDatee;
	public int getEmployeeManagerId() 
	{
		return employeeManagerId;
	}
	public void setEmployeeManagerId(int employeeManagerId) 
	{
		this.employeeManagerId = employeeManagerId;
	}
	public int getManagerDeptNumber() 
	{
		return managerDeptNumber;
	}
	public void setManagerDeptNumber(int managerDeptNumber)
	{
		this.managerDeptNumber = managerDeptNumber;
	}
	public String getFromDatee() {
		return fromDatee;
	}
	public void setFromDatee(String fromDatee) 
	{
		this.fromDatee = fromDatee;
	}
	public String getToDatee() 
	{
		return toDatee;
	}
	public void setToDatee(String toDatee) 
	{
		this.toDatee = toDatee;
	}
}
