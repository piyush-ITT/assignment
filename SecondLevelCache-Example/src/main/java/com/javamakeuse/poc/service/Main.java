package com.javamakeuse.poc.service;

import java.util.Scanner;

import org.hibernate.Session;

import com.javamakeuse.poc.pojo.Country;
import com.javamakeuse.poc.util.HibernateUtility;

public class Main {
	private static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) 
	{
		storeData();  // Method for taking input from user
		
		//getting session object from session factory
		Session session = HibernateUtility.getSessionFactory().openSession();

		session.beginTransaction();

		Country country = null;
		System.out.println("Enter the id of country:");
		long cid = sc.nextLong();
		
		// Getting information in country object using country id
		country = (Country) session.load(Country.class, cid);
		
		System.out.println("Country from the Database => "+country);
		System.out.println("----------------------------------------");
	    System.out.println("   Cid    |     code      |    name    |");
	    System.out.println("----------------------------------------");
	    System.out.println(country.getCountryID() + "\t\t" + country.getCountryCode() 
		  					+ "\t\t" + country.getCountryName());

		System.out.println("Going to print Country *** from First Level Cache");
		// second time loading same entity from the first level cache
		country = (Country) session.load(Country.class, cid);
		System.out.println(country);
		System.out.println("----------------------------------------");
	    System.out.println("   Cid    |     code      |    name    |");
	    System.out.println("----------------------------------------");
		  System.out.println(country.getCountryID() + "\t\t" + country.getCountryCode() 
		  + "\t\t" + country.getCountryName());
		
		// removing country object from the first level cache.
		session.evict(country);
		System.out.println("Object removed from the First Level Cache");
		System.out.println();
		System.out.println("Going to print Country *** from Second level Cache");
		country = (Country) session.load(Country.class, cid);
		System.out.println(country);
		System.out.println("----------------------------------------");
	    System.out.println("   Cid    |     code      |    name    |");
	    System.out.println("----------------------------------------");
		  System.out.println(country.getCountryID() + "\t\t" + country.getCountryCode() 
		  + "\t\t" + country.getCountryName());
		session.getTransaction().commit();

		// loading object in another session
		Session session2 = HibernateUtility.getSessionFactory().openSession();
		session2.beginTransaction();
		System.out.println();
		System.out.println("Printing Country *** from Second level Cache in another session");
		country = (Country) session2.load(Country.class, cid);
		System.out.println(country);
		System.out.println("----------------------------------------");
	    System.out.println("   Cid    |     code      |    name    |");
	    System.out.println("----------------------------------------");
		  System.out.println(country.getCountryID() + "\t\t" + country.getCountryCode() 
		  + "\t\t" + country.getCountryName());
		session2.getTransaction().commit();

	}
	 private static void storeData()
	    {
	        Session session = HibernateUtility.getSessionFactory().openSession();
	        session.beginTransaction();
	         
	        Country country = new Country();
	        System.out.println("Enter Country code:");
	        country.setCountryCode(sc.next());
	        System.out.println("Enter Country name:");
	        country.setCountryName(sc.next());
	         
	        session.save(country);
	        session.getTransaction().commit();
	    }  
}
