package com.operation;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.domain.Faculty;

public class InsertFaculty 
{
	public static void main(String[] args) 
	 {
		
	  //creating configuration object  
      Configuration cfg=new Configuration();  
      cfg.configure("hibernate.cfg.xml"); //populates the data of the configuration file   
	  
	  //Creating session factory object
	  SessionFactory factory=cfg.buildSessionFactory();
	  //SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	  
	  //getting session object from session factory	  
	  Session session = factory.openSession();
	  
	  //getting transaction object from session object
	  session.beginTransaction();
	  
	  //Create faculty entity object
	  Faculty faculty = new Faculty();
	  faculty.setFirst_name("Laxman");
	  faculty.setLast_name("Mali");
	  faculty.setEmail("lax@gmail.com");
	  faculty.setPassword("123456789");
	  faculty.setPhone("12345678");
	  faculty.setGender("male");	
	  
	  session.persist(faculty);//persisting the object 
	  
	  //session.save(faculty);	  
	  session.getTransaction().commit();
	  System.out.println("Inserted Successfully");
	  session.close();
	  factory.close();
	 }
}
