package com.javacodegeeks.dao;

import static org.junit.Assert.*;

import java.util.List;

import javax.transaction.Transactional;
import org.hibernate.SessionFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import com.javacodegeeks.configuration.HibernateConfiguration;
import com.javacodegeeks.model.Faculty;
import com.javacodegeeks.service.FacultyService;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=HibernateConfiguration.class)
@WebAppConfiguration
public class FacultyDaoImplTest 
{
	@Autowired
	FacultyService facultyService;
	
	@Autowired
    private SessionFactory sessionFactory;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
		System.out.println("-----> SETUP <-----");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception 
	{
		System.out.println("-----> DESTROY <-----");
	}
	
	@Test
	@Rollback
	public void testRegisterEmailNotExists() 
	{
		Faculty faculty = new Faculty();
        faculty.setFirst_name("piysh");
        faculty.setLast_name("tiwari");
        faculty.setEmail("p@gmail.com");
        faculty.setPassword("1234567890");
        faculty.setPhone("1234567890");
        faculty.setGender("male");
		if(faculty != null) 
		{
			assertNotNull("faculty first name can't be null", faculty.getFirst_name());
            assertNotNull("faculty Last name can't be null", faculty.getLast_name());
			assertNotNull("Email must not be empty", faculty.getEmail());
            assertNotNull("faculty Password can't be null", faculty.getPassword());
            assertNotNull("faculty phone number can't be null", faculty.getPhone());
            assertNotNull("faculty gender can't be null", faculty.getGender());
		}
		Faculty faculty1 = (Faculty) sessionFactory.getCurrentSession().get(Faculty.class, faculty.getEmail());
		System.out.println(faculty1);
		assertNotNull("New Faculty Information is null", faculty);
		assertNull("email id already exists", faculty1);
		sessionFactory.getCurrentSession().save(faculty);
	}
	
	@Test
	public void testRegisterFail() 
	{
		Faculty faculty = new Faculty();
        faculty.setLast_name("tiwari");
        faculty.setEmail("p@gmail.com");
        faculty.setPassword("1234567890");
        faculty.setPhone("1234567890");
        faculty.setGender("male");
		if(faculty != null) 
		{
			assertNull("faculty first name can't be null", faculty.getFirst_name());
            assertNotNull("faculty Last name can't be null", faculty.getLast_name());
			assertNotNull("Email must not be empty", faculty.getEmail());
            assertNotNull("faculty Password can't be null", faculty.getPassword());
            assertNotNull("faculty phone number can't be null", faculty.getPhone());
            assertNotNull("faculty gender can't be null", faculty.getGender());
		}
		assertNotNull("New Faculty Information is not null", faculty);
	}
	
	@Test(expected=org.hibernate.id.IdentifierGenerationException.class)
	public void IdentifierGenerationExceptionTest()
	{
		Faculty faculty = new Faculty();
		facultyService.register(faculty);
	}
	
	@Test
	public void testFindAll() throws Exception
	{
		@SuppressWarnings("unchecked")
		List<Faculty> faculties = sessionFactory.getCurrentSession().createCriteria(Faculty.class).list();
		assertNotNull("faculty object is null", faculties);
	}
	
	@Test
	public void testGetFacultyDataPass() 
	{
	   String email = "pappu@gmail.com";
	   Faculty faculty = (Faculty) sessionFactory.getCurrentSession().get(Faculty.class, email);		
       assertNotNull("faculty object is null", faculty);
       assertEquals("pappu@gmail.com", faculty.getEmail());
	}
	
	@Test
	public void testGetFacultyDataFail() 
	{
	   String email = "p@gmail.com";
	   Faculty faculty = (Faculty) sessionFactory.getCurrentSession().get(Faculty.class, email);		
       assertNull("faculty object is null", faculty);
	}

	@Test(expected = java.lang.NullPointerException.class)
	@Rollback
	public void testDeleteFacultyException() 
	{
	   String email = "hvh@gmail.com";
	   Faculty faculty=new Faculty();
       faculty.setEmail(email);
       sessionFactory.getCurrentSession().delete(faculty);
       Faculty faculty1 = (Faculty) sessionFactory.getCurrentSession().get(Faculty.class, email);
       System.out.println(faculty1.getEmail());
	}
	
	@Test
	public void testDeleteFacultyFail() 
	{
	   String email = "p@gmail.com";
	   Faculty faculty=new Faculty();
       faculty.setEmail(email);
       sessionFactory.getCurrentSession().delete(faculty);
       Faculty faculty1 = (Faculty) sessionFactory.getCurrentSession().get(Faculty.class, email);
       assertNull("Email not exists in database", faculty1);
	}

	@Test
	@Rollback
	public void testUpdateFacultyDataPass() 
	{
		Faculty faculty = new Faculty();
		faculty.setFirst_name("Mukesh");
        faculty.setLast_name("tiwari");
        faculty.setEmail("pappu@gmail.com");
        faculty.setPassword("1234567890");
        faculty.setPhone("1234567890");
        faculty.setGender("male");
		sessionFactory.getCurrentSession().update(faculty);
		assertEquals("Mukesh", faculty.getFirst_name());
	}
	
	@Test
	@Rollback
	public void testUpdateFacultyDataFail() 
	{
		Faculty faculty = new Faculty();
		faculty.setFirst_name("Mukesh");
        faculty.setLast_name("tiwari");
        faculty.setEmail("pappu@gmail.com");
        faculty.setPassword("1234567890");
        faculty.setPhone("1234567890");
        faculty.setGender("male");
		sessionFactory.getCurrentSession().update(faculty);
		assertNotEquals("Update in database failed!!", "Mukes", faculty.getFirst_name());
	}

}
