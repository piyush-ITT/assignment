/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javacodegeeks.controller;

import com.javacodegeeks.model.Faculty;
import com.javacodegeeks.model.Login;
import com.javacodegeeks.service.FacultyService;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * @author piyush.tiwari
 */

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping("/")
@ComponentScan("com.javacodegeeks")
public class HomeController 
{
    @Autowired
	FacultyService facultyService;
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Locale locale, Model model) 
        {
		logger.info("Welcome to the ITT!");		
		model.addAttribute("itt", "This is sample message" );
		
		return "index";
	}
	
        @RequestMapping(value = "/login", method = RequestMethod.GET)
        public ModelAndView showLogin(HttpServletRequest request, HttpServletResponse response) 
        {
        ModelAndView mav = new ModelAndView("login");
        mav.addObject("login", new Login());
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
        return mav;
        }
        
        /*
        Method checking for authentication at login time
        */	    
        @RequestMapping(value = "/loginProcess", method = RequestMethod.POST, headers = "Accept=application/json")
        public ModelAndView loginProcess(HttpServletRequest request, HttpServletResponse response,@ModelAttribute("login") Login login) 
        {
            logger.info("inside the login process");
            ModelAndView mav = null;
            Faculty faculty = facultyService.getFaculty(login);
            if (null != faculty) 
            {
                List<Faculty> faculties = facultyService.findAll();

                mav = new ModelAndView("home");
                mav.addObject("first_name", faculty.getFirst_name());
                mav.addObject("list", faculties);
            } 
            else 
            {
            mav = new ModelAndView("login");
            mav.addObject("message", "Username or Password is wrong!!");
            }
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 0);
            return mav;

        }


          @RequestMapping(value = "/register", method = RequestMethod.GET)
          public ModelAndView showRegister(HttpServletRequest request, HttpServletResponse response) 
          {
            ModelAndView mav = new ModelAndView("register");
            mav.addObject("faculty", new Faculty());
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 0);
            return mav;
          }
          
          /*
          Method checking for validation at server side and registering user;
          redirect to homepage if everything is correct
          */
          @RequestMapping(value = "/registerProcess", method = RequestMethod.POST, headers = "Accept=application/json")
          public ModelAndView registerProcess(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute("faculty") Faculty faculty, BindingResult bindingResult ) 
          { 
            ModelAndView mav;
            logger.info("inside the registration process37");

            if (!bindingResult.hasErrors())
            {
                facultyService.register(faculty);
                mav = new ModelAndView("home", "faculty", faculty.getFirst_name());
                List<Faculty> faculties = facultyService.findAll();
                mav.addObject("list", faculties);
                response.setHeader("Cache-Control", "no-cache");
                response.setHeader("Cache-Control", "no-store");
                response.setHeader("Pragma", "no-cache");
                response.setDateHeader("Expires", 0);
                return mav;
            }
            else
            {
                mav = new ModelAndView("register");
                return mav;
            }

        }
        /*
        Method to show all data in Json format
        */
        @ResponseBody
        @RequestMapping(value="/nextpage" ,method = RequestMethod.GET, headers = "Accept=application/json")
        public List <Faculty> getList()
        {
            return facultyService.findAll();
        }

        /*
        Method to invalidate session after logout
        */      
        @RequestMapping(value = "/logout", method = RequestMethod.GET)
        public String logout(HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) 
        {
            logger.info("inside logout!");
            redirectAttributes.addFlashAttribute("logoutMessage", "Successfully logged out!!");
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (auth != null)
            {    
                new SecurityContextLogoutHandler().logout(request, response, auth);
            }

            return "redirect:/login";//You can redirect wherever you want, but generally it's a good practice to show login screen again.
        } 
        
         /*
        Method for displating specific faculty information in JSON
        */
        @ResponseBody
        @RequestMapping(value="facultydata/{email}" ,method = RequestMethod.GET, headers="Accept=application/json")
        public Faculty getFacultyData(@PathVariable("email") String email)
        {
            Faculty fac = facultyService.getFacultyData(email + ".com");
            return fac;
        }

        /*
        Method for deleting faculty information via email
        */
        @ResponseBody
        @RequestMapping(value="delete/{email}" ,method = RequestMethod.GET, headers="Accept=application/json")
        public String deleteFaculty(@PathVariable("email") String email)
        {
            facultyService.deleteFaculty(email + ".com");
            return "Deleted!!";
        }

        /*
        Method for updating Faculty information
        */
        @ResponseBody
        @RequestMapping(value="/update" ,method = RequestMethod.PUT, headers="Accept=application/json")
        public String updateFaculty(@RequestBody Faculty faculty) 
        {
            facultyService.updateFacultyData(faculty);
            return "Updated";
        }
}
