/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javacodegeeks.dao;

import com.javacodegeeks.model.Faculty;
import com.javacodegeeks.model.Login;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author piyush.tiwari
 */
@Repository
@Transactional
public class FacultyDaoImpl implements FacultyDao 
{
    @Autowired
    private SessionFactory sessionFactory;
        
    // Inserting faculty information in database
    public void register(Faculty faculty) 
    {
        sessionFactory.getCurrentSession().saveOrUpdate(faculty);
    }

    // Method selecting all the information and showing to the user after log in
    public List<Faculty> findAll() 
    {
        return (List<Faculty>) sessionFactory.getCurrentSession().createCriteria(Faculty.class).list();

    }

    // Method checking authentication at login time
    public Faculty getFaculty(Login login) 
    {
       return (Faculty) sessionFactory.getCurrentSession().get(Faculty.class, login.getUsername());
    }

     // Method for getting single faculty data
    public Faculty getFacultyData(String email) 
    {
       return (Faculty) sessionFactory.getCurrentSession().get(Faculty.class, email);
    }

    // Method for deleting faculty information
    public void deleteFaculty(String email) 
    {
        Faculty faculty=new Faculty();
        faculty.setEmail(email);
        sessionFactory.getCurrentSession().delete(faculty);
    }

    public void updateFacultyData(Faculty faculty) 
    {
        sessionFactory.getCurrentSession().update(faculty);
    }
}
