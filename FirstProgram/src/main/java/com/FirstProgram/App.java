package com.FirstProgram;

import java.util.Scanner;

/**
 * 
 * @author piyush.tiwari
 *
 */

public class App
{
	public static int num;
	private static Scanner sc;
	public static int count = 0;
	
	 static int ab=0;
	
	public static void main(String[] args) throws InterruptedException
	{	
		App a =new App();
		sc = new Scanner(System.in);
		// Taking input from user
		System.out.println("Enter the number of thread you want to create");
		num = sc.nextInt();
		for ( int i = 0; i < num; i++ )
		{
			CountClass countClass = new CountClass(a);  // passing object of main class
			Thread t = new Thread(countClass);
			ab++;   					// Counting how many no. of threads are creating
			t.start();			
		}
		System.out.println("No. of threads created: " + ab);
		synchronized (a) 
		{
			a.wait();					// Suspending Main class thread
			System.out.println("count :" + count);
			
		}
	}
	
}
	
	

