package com.FirstProgram;

public class CountClass implements Runnable 
{
	
	static int i=0;
	App b;
	public CountClass(App ap) 
	{
		// TODO Auto-generated constructor stub
		b=ap;
	}
	public void run() 
	{			
		synchronized (b) 
		{
			App.count+=10;		// incrementing count variable by +10 after each time the thread is executing   
			if(++i == App.ab)
			{
				b.notify();		// Notifying Main class thread if above condition is satisfied
			}
		}
					
	}
}
