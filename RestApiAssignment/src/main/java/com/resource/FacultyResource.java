
package com.resource;

import com.facultyDao.FacultyDao;
import com.restapiassignment.Faculty;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author piyush.tiwari
 */


@Path("/faculties")
public class FacultyResource 
{
    
    /*
    Method for displaying all the faculty information in JSON
    */
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<Faculty> findAll() 
    {
        FacultyDao dao = new FacultyDao();
        List faculties = dao.findAll();
        return faculties;
    }
    
    /*
    Method for registering Faculty Information
    */
    
    @POST
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void register(@FormParam("first_name") String first_name,
			@FormParam("last_name") String last_name,
			@FormParam("email") String email,
                        @FormParam("password") String password,
                        @FormParam("phone") String phone,
                        @FormParam("gender") String gender,
			@Context HttpServletResponse servletResponse) throws IOException 
    {
        register(first_name, last_name, email, password, phone, gender);
        //return Response.ok().build();
        servletResponse.sendRedirect("./faculties/");
    }
    
    
    
    public  void register(String first_name, String last_name, String email, String password, String phone, String gender)
    {
        Faculty faculty1 = new Faculty();
        faculty1.setFirst_name(first_name);
        faculty1.setLast_name(last_name);
        faculty1.setEmail(email);
        faculty1.setPassword(password);
        faculty1.setPhone(phone);
        faculty1.setGender(gender);
                
        FacultyDao dao = new FacultyDao();
        dao.register(faculty1);
        //return Response.ok().build();
    }
        
    /*
    Method for displating specific faculty information in JSON
    */
    @GET
    @Path("facultydata/{email}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Faculty getFacultyData(@PathParam("email") String email)
    {
        FacultyDao dao = new FacultyDao();
        return dao.getFacultyData(email);
    }
        
    /*
    Method for deleting faculty information via email
    */
    @DELETE
    @Path("/delete/{email}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteFaculty(@PathParam("email") String email)
    {
        FacultyDao dao = new FacultyDao();
        dao.deleteFaculty(email);
        return Response.ok().build();
    }
    
    /*
    Method for updating Faculty information
    */
    @PUT
    @Path("/update")
    @Consumes("application/json")
    public Response updateFaculty(Faculty faculty) 
    {
        FacultyDao dao = new FacultyDao();
        dao.updateFacultyData(faculty);
        return Response.ok().build();
    }
}

