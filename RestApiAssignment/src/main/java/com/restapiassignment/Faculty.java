
package com.restapiassignment;

/**
 *
 * @author piyush.tiwari
 */
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/*
Faculty persistence class
*/
@Entity
@Table(name="facultyInformation")
public class Faculty implements Serializable 
{
	@Column(name = "first_name")
	@Size(min=2, max=30, message = "First name must be greater than 1")
	private String first_name;
	
	@Column(name = "last_name")
	@Size(min=2, max=30, message = "Last name must be greater than 1")
	private String last_name;
	
	@Id @Column(name="email")
	@NotEmpty(message = "Email must not be empty") @Email
	private String email;
	
	@Column(name="password")
	@Size(min=8, max=30, message = "password must be greater than 7")
	private String password;
	
	@Column(name="phone")
	@Size(min=10, max=20, message = "phone number must be greater than 9")
	private String phone;
	
	@Column(name="gender")
	private String gender;

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
}

