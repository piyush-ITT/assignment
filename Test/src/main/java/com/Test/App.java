package com.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 
 * @author piyush.tiwari
 *
 */

class WorkerThread implements Runnable 
{
    private String message;
    public WorkerThread(String s) // Constructor of WorkerThread
    {
        this.message = s;
    }
 
    public void run() 
    {
        System.out.println(Thread.currentThread().getName() + " (Start) message = " + message);
        processmessage();
        System.out.println(Thread.currentThread().getName() + " (End)");
    }
 
    private void processmessage()
    {
        try 
        {
        	Thread.sleep(2000);  // Sleep thread for 2sec
        } catch (InterruptedException e) 
        {
        	e.printStackTrace(); 
        }
    }
   
}


public class App 
{
 
    public static void main(String[] args) 
    {
        ExecutorService executor = Executors.newFixedThreadPool(5); // Created Threadpool of size 5
        for (int i = 0; i < 10; i++) 
        {
            Runnable worker = new WorkerThread("" + i);
            executor.execute(worker);   // Providing task to thread via execute Method in executor
          }
        executor.shutdown();
        while (!executor.isTerminated()) // Chrcking for termination of all threads
        {
        }
        System.out.println("Finished all threads");
    }
 
}