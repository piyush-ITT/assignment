package com.HighestFactor;

public class Factor implements Runnable
{
	private int lb, ub;  	//Stores the Upper and Lower Value 

	public Factor(int lower, int upper)
		{
			lb = lower;
			ub = upper;
		}

	// Method Contains the logic for execution in Threads
	public void run()
		{
		int []arr=new int[1000];
		int max = arr[0];
			for ( int i = lb, k=0; i <= ub; i++,k++)
				{
					if(i == 1)
						arr[k]=0;
					else
					{
						for (int j = 2; j <= i/2; j++)
							{
								if (i % j == 0)
									{
										arr[k]++;
									}
							}
						if (arr[k] > max)
							max = arr[k];			// Comparing and Storing maximum no. of divisor
					}
				}
			Max(arr,max);
		}
	// Method for storing which number has maximum no. of divisor 
	private void Max(int[] arr, int max) 
	{
		// TODO Auto-generated method stub	
		for (int i = 0,j=lb; i < 1000; i++,j++)
		{
			NumberAndDivisor numberAndDivisor = new NumberAndDivisor();
			if(arr[i] == max)
				numberAndDivisor.setDivisor(max);
				numberAndDivisor.setNumber(j);
				HighestFactor.digi.add(numberAndDivisor);
		}
	}

}
