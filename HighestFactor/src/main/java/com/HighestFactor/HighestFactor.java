package com.HighestFactor;

import java.util.ArrayList;

/**
 * 
 * @author piyush.tiwari
 *
 */

public class HighestFactor 
{
	// ArrayList for storing which number has maximum divisor
	static  ArrayList<NumberAndDivisor> digi = new ArrayList<NumberAndDivisor>();
	public static void main(String[] args) throws InterruptedException
		{
			Thread []thread = new Thread[11];

			for (int i = 1; i <= 10; i++)
				{
					Factor f = new Factor(((i - 1) * 1000 + 1),i * 1000);
					thread[i] = new Thread(f);	//Creates the multiple threads 						
					thread[i].start(); //creating thread
					thread[i].join();
				}			
			int max =digi.get(0).getDivisor();   // taking first element from ArrayList
			for(int i = 1; i < digi.size(); i++)
			{
				if ( digi.get(i).getDivisor() > max)
					max = digi.get(i).getDivisor();
			}
			System.out.println("Following numbers have higest number of Divisors:-");					
			for (int i = 0; i < digi.size(); i++)		// Loop for printing numbers who have highest number of divisors
				{
					if (max == digi.get(i).getDivisor()) 
						System.out.println("Number " + digi.get(i).getNumber() + " has " + (max) + " number of Factors ");
								
				}
		}

}
