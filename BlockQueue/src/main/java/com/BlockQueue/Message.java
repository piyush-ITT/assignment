package com.BlockQueue;

public class Message 
{
    private String msg;
    
    public Message(String str)  // Constructor of Message
    {
        this.msg = str;
    }

    public String getMsg()  // Getter method
    {
        return msg;
    }

}
