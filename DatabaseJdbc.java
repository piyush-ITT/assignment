package com.JdbcDatabase;

import java.util.Scanner;
import java.sql.*;

public class DatabaseJdbc 
{
//	private static final String DB_DRIVER = "oracle.jdbc.driver.OracleDriver";
	private static final String DB_CONNECTION = "jdbc:mysql://localhost:3306/employeedatabase";
	private static final String DB_USER = "root";
	private static final String DB_PASSWORD = "gameover";
	private static Scanner sc;
	static Connection connection = null ;
	public static void main(String[] args) throws SQLException 
	{	
		int i;
		try 
		{
		 // Drivers 
		  Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) 
		{
			System.out.println("Where is your MySQL JDBC Driver?");
			e.printStackTrace();
			return;
		}
		System.out.println("MySQL JDBC Driver Registered!");

		try 
		{
			// Creating Connection 
			connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);

		} catch (SQLException e) 
		{
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;
		}
        sc = new Scanner(System.in);
        do 
        {
        	//Menu which will show to user initially
	        System.out.println("Welcome enter the following operation you want to perform on database:-");
	        System.out.println("Enter 6 for Exit");
	        System.out.println("1. Create");
	        System.out.println("2. Insert");
	        System.out.println("3. Select");
	        System.out.println("4. Update");
	        System.out.println("5. Delete");
	        i = sc.nextInt();
	            switch(i)
	            {
		             case 1:
		              	 createUserDbTable(); // Method for Create table
		                 break;
		             case 2:
		            	 insertRow(); // Method for Insert row
		                 break;
		             case 3:
		            	 selectData();// Method for Select data
		            	 break;
		             case 4:
		            	 updateData();// Method for Update
		            	 break;
		             case 5:
		            	 deleteUserDbTable();// Method for deleting table
		            	 break;
		             case 6:
		            	 System.exit(1);
		             default:
		                 System.out.println("-----------invalid input-----------\n");
	
	            }
	    }while (i != 0);
    }
	// Definition for Method to Update
	private static void updateData()
	{
		// TODO Auto-generated method stub
		String table_name, column_name, primary_key, data, primaryKeyValue;
        System.out.println("Enter name of Table");
        sc.nextLine();
        table_name = sc.nextLine();
        System.out.println("Enter column  name");
        column_name = sc.nextLine();
        System.out.println("Enter data");
        data = sc.nextLine();
        System.out.println("Enter primary key name");
        primary_key = sc.nextLine();
        System.out.println("Enter primary key value");
        primaryKeyValue = sc.nextLine();
        String str = "update " + table_name + " set " + column_name + " ='" + data + "' where " + primary_key + " = " + primaryKeyValue;

        System.out.println(str);
        try 
        {
            PreparedStatement ps = connection.prepareStatement(str);
            ps.executeUpdate();
        } catch (SQLException ex) 
        {
            System.out.println(ex.getMessage());
        }
    }
	// Definition for Method to delete table
    private static void deleteUserDbTable() 
    {
        String table_name;
        System.out.println("Enter name of Table");
        table_name = sc.next();
        String str = "drop table " + table_name;
        try
        {
            PreparedStatement ps = connection.prepareStatement(str);
            ps.executeUpdate();
        } catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
        }
	}
    
    // Definition for Method to select data
	private static void selectData() 
	{
		// TODO Auto-generated method stub
		String table_name, str;
        System.out.println("Enter table Name");
        sc.nextLine();
        table_name = sc.nextLine();
        str = "select *from " + table_name + "";
        try {
            PreparedStatement ps = connection.prepareStatement(str);
            ResultSet rs = ps.executeQuery();
            if (table_name.equals("employees")) {
                System.out.println("-------------+---------------+-----------+----------+--------+--------------+\n"
			                        + " employee_id | first_name | last_name | birth_date | gender | hire_date |\n"
			                        + "-------------+---------------+-----------+----------+--------+--------------+");
                while (rs.next()) {
                    System.out.println(rs.getInt("employee_id") + "\t" + "\t" + rs.getString("first_name") + "\t" + rs.getString("last_name") + rs.getDate("birth_date") + "\t" + rs.getString("gender") + "\t" + rs.getDate("hire_date"));
                }
            }  else if (table_name.equals("departments")) {
                System.out.println("+---------------+----------+\n"
			                        + "| dept_no | dept_name   |\n"
			                        + "+---------------+-------+");
                while (rs.next()) {
                    System.out.println("\t" + rs.getInt("dept_no") + "\t" + rs.getString("dept_name"));
                }
            } else if (table_name.equals("dept_manager")) {
                System.out.println("+----------------------+--------------------------+------------+------------+\n"
			                        + "| manager_dept_no | manager_employee_id | from_date  | to_date    |\n"
			                        + "+----------------------+--------------------------+------------+------------+");
                while (rs.next()) {
                    System.out.println(rs.getInt("manager_dept_no") + "\t" + rs.getInt("manager_employee_id") + "\t" + rs.getDate("from_date") + "\t" + rs.getDate("to_date"));
                }
            } else if (table_name.equals("emp_dept")) {

                System.out.println("+---------------------------------+-----------------------------------+------------+------------+\n"
			                        + "| dept_employee_Id | dept_dept_no | from_date  | to_date    |\n"
			                        + "+---------------------------------+-----------------------------------+------------+------------+");
                while (rs.next()) {
                    System.out.println("\t\t" + rs.getInt("dept_employee_Id") + "\t\t\t" + rs.getInt("dept_dept_no") + "\t\t" + rs.getDate("from_date") + "\t" + rs.getDate("to_date"));
                }
            }
            System.out.println(str);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

	}
	// Definition for Method to insert new row
	private static void insertRow() 
	{
		// TODO Auto-generated method stub
		 System.out.println("Enter name of Table");
	        sc.nextLine();
	        String tableName = sc.nextLine();
	        if (tableName.equalsIgnoreCase("employees")) {
	            employees();
	        } else if (tableName.equalsIgnoreCase("emp_dept")) {
	            empDept();
	        } else if (tableName.equalsIgnoreCase("dept_manager")) {
	            deptManager();
	        } else if (tableName.equalsIgnoreCase("departments")) {
	            departments();
	        }
	}

	private static void departments() 
	{
		// TODO Auto-generated method stub
		String str, dept_name;
		int dept_no;
        str = "Insert into departments (dept_no,dept_name) values (?,?)";
        System.out.println("Enter Department number");
        dept_no = sc.nextInt();
        System.out.println("Enter Department Name");
        sc.nextLine();
        dept_name = sc.nextLine();
       // System.out.println(str);
        try {
            PreparedStatement ps = connection.prepareStatement(str);
            ps.setInt(1, dept_no);
            ps.setString(2, dept_name);
            ps.executeUpdate();
            System.out.println("-----data inserted!-----\n");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
	}

	private static void deptManager() 
	{
		// TODO Auto-generated method stub
		String str, fromDate, toDate;
		int deptId;
        int employeeId;
        str = "Insert into dept_manager (manager_dept_no,manager_employee_id,from_date,to_date) values (?,?,?,?)";
        System.out.println("Enter department number");
        employeeId = sc.nextInt();
        System.out.println("Enter manager_employee_id");
        deptId = sc.nextInt();
        System.out.println("Enter the joining date");
        sc.nextLine();
        fromDate = sc.nextLine();
        System.out.println("Enter the last working date");
        toDate = sc.nextLine();
        System.out.println(str);
        try 
        {      	
            PreparedStatement ps = connection.prepareStatement(str);
            ps.setInt(2, employeeId);
            ps.setInt(1, deptId);
            ps.setDate(3, Date.valueOf(fromDate));
            ps.setDate(4, Date.valueOf(toDate));
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
	}

	private static void empDept() 
	{
		// TODO Auto-generated method stub
		String joiningDate, lastWorkingDate, str;
        int employeeId,departmentId;
        str = "Insert into emp_dept (department_employee_id,dept_dept_no,from_date,to_date) values (?,?,?,?)";
        System.out.println("Enter Employee Id");
        employeeId = sc.nextInt();       
        System.out.println("Enter Department Id");
        departmentId = sc.nextInt();
        System.out.println("Enter the joining date of Employee");
        sc.nextLine();
        joiningDate = sc.nextLine();
        System.out.println("Enter the last workingdate");
        lastWorkingDate = sc.nextLine();

        try {
            PreparedStatement ps = connection.prepareStatement(str);
            ps.setInt(1, employeeId);
            ps.setInt(2, departmentId);
            ps.setDate(3, Date.valueOf(joiningDate));
            ps.setDate(4, Date.valueOf(lastWorkingDate));
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
	}

	private static void employees() 
	{
		// TODO Auto-generated method stub
		String fname, lname, joiningDate, birthDate, gender, str;
        str = "Insert into employees (first_name,last_name,birth_date,gender,hire_date) values (?,?,?,?,?)";
        System.out.println("Enter the first name of Employee");
        sc.nextLine();
        fname = sc.nextLine();
        System.out.println("Enter the last name of Employee");
        lname = sc.nextLine();
        System.out.println("Enter the birth date of Employee");
        birthDate = sc.nextLine();
        System.out.println("Enter the gender of Employee");
        gender = sc.nextLine();
        System.out.println("Enter the hire date of Employee");
        joiningDate = sc.nextLine();    

        try {
            PreparedStatement ps = connection.prepareStatement(str);
            ps.setDate(1, Date.valueOf(birthDate));
            ps.setString(2, fname);
            ps.setString(3, lname);
            ps.setString(4, gender);
            ps.setDate(5, Date.valueOf(joiningDate));
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
	}	
	// InsertRow all methods end here
	
	// Definition for Method to create table
	private static void createUserDbTable() throws SQLException 
	{
		int flag=0,flag1=0;
		String column_name=null;
		String data_type=null;
		String all_column="";
		// TODO Auto-generated method stub
		System.out.println("Enter table name you want to create:");
		sc.nextLine();
		String table_name = sc.nextLine();
		System.out.println("Enter no. of column you want to create in table:");
		int no_of_column = sc.nextInt();
		sc.nextLine();
		for(int k=1;k<no_of_column;k++)
		{
			System.out.println("Enter the Column name:");			
        	column_name = sc.nextLine();
        	System.out.println("Enter column data type in given format: Data_type/Data_type(size)");
        	data_type = sc.nextLine();
			all_column=all_column+" "+column_name+" "+data_type+",";
		}
		System.out.println("Enter the Column name:");			
    	column_name = sc.nextLine();
    	System.out.println("Enter column data type in given format: Data_type/Data_type(size)");
    	data_type = sc.nextLine();
			
    	 String str,primary_key = null;
         System.out.println("Do you want to make primary key?(y/n)");
         str = sc.next();
         if(str.equalsIgnoreCase("y"))
         {
         	System.out.println("Enter the Column name which you want to make primary key:");
         	sc.nextLine();
         	primary_key = sc.nextLine();
         	flag=1;
         }
         
         String str1,foreign_key = null,foreign_table = null,foreign_column = null;
         System.out.println("Do you want to add foreign key?(y/n)");
         str1 = sc.next();
         if(str1.equalsIgnoreCase("y"))
         {
         	System.out.println("Enter the Column name which you want to make foreign key:");
         	sc.nextLine();
         	foreign_key = sc.nextLine();
         	System.out.println("Enter the table name which you want to connect:");
         	foreign_table = sc.nextLine();
         	System.out.println("Enter the Column name of foreign table:");
         	foreign_column = sc.nextLine();
         	flag1=1;
         }
         
         Statement statement = null;
         String createTableSQL=null;             
         if(flag==1 && flag1==1)
         {
         createTableSQL = "CREATE TABLE " + table_name + "("
 				        + all_column +column_name+" "+data_type+" ,PRIMARY KEY("+primary_key+"), "
 				       	+ "FOREIGN KEY("+foreign_key+") "+ "REFERENCES " 
 						+ foreign_table+"("+foreign_column+"));";
         }
         else if(flag==1)
         {
        	 createTableSQL = "CREATE TABLE " + table_name + "("
      						+ all_column +column_name+" "+data_type+" ,PRIMARY KEY("+primary_key+"));";
        	 
         }
         else if(flag1==1)
         {
        	 createTableSQL = "CREATE TABLE " + table_name + "("
       						+ all_column + column_name+" "+data_type+" ,FOREIGN KEY("+foreign_key+") "
       						+ "REFERENCES " 
     						+ foreign_table+"("+foreign_column+"));"; 
         }
         
         else if(flag==0)
         {
        	 createTableSQL = "CREATE TABLE " + table_name + "("
      				+ all_column+column_name+" "+data_type+");";
        	 
         }
         
         statement = connection.createStatement();

		System.out.println(createTableSQL);
                     // execute the SQL statement
		statement.execute(createTableSQL);

		System.out.println("-----Table is created!------\n");
 

		if (statement != null)
		{
			statement.close();
		}
	}

}
